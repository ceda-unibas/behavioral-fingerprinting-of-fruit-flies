"""Dataset processing module"""
import h5py
import pandas as pd

def parse_h5py(files, verbose=True):
    """Parses the h5 files and returns the X,Y coordinates of the fly in the form of pandas dataframe, along with the node names.
    
    Parameters
     ----------
    files : list
        The list containing the h5 file-paths.

    Returns
    -------
    df : pandas.dataframe
        It contains the X,Y coordinates of the fly in the form of pandas dataframe, along with the node names.
    node_names : list
        It contains the names of the nodes (landmark points) of the fly skeleton.
    
    """
 
    df_files = []

    for file in files:
        with h5py.File(file, "r") as f:
            dset_names = list(f.keys())
            locations = f["tracks"][:].T
            node_names = [n.decode() for n in f["node_names"][:]]

        if verbose:
            print("===filename===")
            print(file)
            print()

            print("===HDF5 datasets===")
            print(dset_names)
            print()

            print("===locations data shape===")
            print(locations.shape)
            print()

            print("===nodes===")
            for i, name in enumerate(node_names):
                print(f"{i}: {name}")

        # Flatten data
        from copy import deepcopy
        data = deepcopy(locations)

        # Flatten the time and points dimensions into a 2D array
        T, P, D, C = data.shape
        if verbose:
            print(T, P,D, C)
        data_x = deepcopy(locations[:,:,0,:])
        data_y = deepcopy(locations[:,:,1,:])
        data_2dx = data_x.reshape(T*P )#.astype(float)
        data_2dy = data_y.reshape(T*P )#.astype(float)

        # Convert the 2D array to a DataFrame
        df_flattenedx = pd.DataFrame(data_2dx)
        df_flattenedy = pd.DataFrame(data_2dy)

        df_flattened = pd.concat([df_flattenedx,df_flattenedy],1)

        df_flattened.columns=['X','Y']

        arr = T*node_names

        df_flattened['node_name'] = arr[:]

        times_arr = []
        for i in range(T):
            times_temp = [i]
            tmp = P*times_temp
            times_arr.extend(tmp)

        df_flattened['timestamp'] = times_arr[:]

        df_files.append(df_flattened)

    return df_files, node_names