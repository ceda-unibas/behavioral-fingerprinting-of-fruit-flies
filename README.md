# behavioral-fingerprinting-of-fruit-flies

This project aims to automatically identify doifferent behavioral patterns of fruit flies. Datasets consist of markerless tracking of flies’ body parts positions. Flies were freely behaving in individual chambers and recorded with a high-resolution camera. 

[![license][license-badge]][license]


## Project status
Ongoing.

## Installation

Create a new conda environment for this project by running the following on your Terminal:

```sh
conda env create -f flais.yml
```

Activate the new environment with `conda activate flais` (or `source activate flais`, depending on your system) and install the necessary dependencies with

```sh
make install
```

Alternatively, you may explicitly run `pip3 install .`

Note that further dependencies may need to be installed if you want to run the notebooks and/or scripts in the `notebooks` and `scripts` directories. For instructions on how to install these dependencies, please refer to the respective README files in those directories.

## Usage
See the notebooks in the `notebooks` directory, and the scripts in the `notebooks` directory for examples and more detailed use cases.

## Authors and acknowledgment
This project is a result of a collaboration between [CeDA][ceda] and [Anissa Kempf's research group][kempf] at the University of Basel.

## License
This project is released under the [BSD 3-Clause License][license].

[ceda]: https://ceda.unibas.ch/
[contributing]: CONTRIBUTING.md
[kempf]: https://www.biozentrum.unibas.ch/research/research-groups/research-groups-a-z/overview/unit/research-group-anissa-kempf
[license]: LICENSE
[license-badge]: https://img.shields.io/badge/license-BSD-green

